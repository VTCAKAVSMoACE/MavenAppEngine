package info.addisoncrump.mae.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class MAESecurityWebApplicationInitializer
    extends AbstractSecurityWebApplicationInitializer {
}
