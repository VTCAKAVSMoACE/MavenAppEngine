package info.addisoncrump.mae.web;

import com.google.api.client.util.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.WritableResource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Objects;

@RestController
public class MAEWebController {
    private final ApplicationContext context;
    private final String bucket;
    private final String redirect;
    private final MultiValueMap<String, String> redirectHeaders;

    public MAEWebController(ApplicationContext context, @Value("${maven-bucket}") String bucket,
                            @Value("${about-page}") String redirect) {
        this.context = context;
        this.bucket = bucket;
        this.redirect = redirect;
        this.redirectHeaders = new LinkedMultiValueMap<>();
        this.redirectHeaders.put("Location", Collections.singletonList(redirect));
    }

    @GetMapping("/")
    public ResponseEntity<String> get() {
        return new ResponseEntity<>(
            String.format("Redirecting to <a href=\"%s\">%s</a> ...", redirect, redirect),
            redirectHeaders,
            HttpStatus.PERMANENT_REDIRECT
        );
    }

    @GetMapping("/**")
    public ResponseEntity<InputStreamResource> get(HttpServletRequest mapping) throws
                                                                               IOException {
        String url = (String) mapping.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
        try {
            Resource resource = context.getResource(String.format("gs://%s%s", bucket, url));
            HttpHeaders headers = new HttpHeaders();
            headers.put(
                "Content-Disposition",
                Collections.singletonList("attachment; filename=" + url.substring(url.lastIndexOf('/') + 1))
            );
            InputStreamResource responseResource = new InputStreamResource(resource.getInputStream());
            return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .contentLength(resource.contentLength())
                .headers(headers)
                .body(responseResource);
        } catch (FileNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping(value = "/**", consumes = MediaType.ALL_VALUE)
    public void put(HttpServletRequest mapping) throws
                                                                               IOException {
        String url = (String) mapping.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
        WritableResource resource = (WritableResource) context.getResource(String.format("gs://%s%s", bucket, url));
        try (OutputStream stream = resource.getOutputStream()) {
            IOUtils.copy(mapping.getInputStream(), stream);
        }
    }
}
